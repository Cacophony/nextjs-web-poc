import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const queryGetBotStats = gql`
  query {
    botStats {
      guilds
      users
    }
  }
`;

const botStatsText = (guilds, users) => {
  return (
    <p>
      The bot is on <b>{guilds ? guilds : "…"}</b> servers, talking to{" "}
      <b>{users ? users : "…"}</b> users.
    </p>
  );
};

const BotStats = () => {
  const { error, data } = useQuery(queryGetBotStats, {
    notifyOnNetworkStatusChange: true
  });
  if (error) return botStatsText();
  if (data && data.botStats) {
    return botStatsText(data.botStats.guilds, data.botStats.users);
  }
  return botStatsText();
};

export default BotStats;
